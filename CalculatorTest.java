import org.junit.*;
import static org.junit.Assert.*;

public class CalculatorTest{

    @Test
    public void testAdd(){
	double n1 = 2.5;
	double n2 = 0;
	double expected = 2.5;
	double result = Calculator.add(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

    @Test
    public void testSubtract(){
	double expectedResult = 5;
	assertEquals(expectedResult, Calculator.subtract(5, 10), 1e-6);
    }

    @Test
    public void testMultiply() {
	double expectedResult = -27.5;
	    assertEquals(expectedResult, Calculator.multiply(-12.5, 2.2), 1e-6);
    }

    @Test
    public void testDivide() {
	double expectedResult = 0;
	assertEquals(expectedResult, Calculator.divide(55, 0), 1e-6);
    }

    @Test
    public void testAbs() {
	double expectedResult = 0.25;
	assertEquals(expectedResult, Calculator.abs(-0.25), 1e-6);
    }

    @Test
    public void testPower() {
	double expectedResult = 32;
	assertEquals(expectedResult, Calculator.power(2, 4), 1e-6);
    }
}
