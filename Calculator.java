
public class Calculator
{
    /* add n2 to n1 */
    static double add(double n1, double n2)
    {
        return n1 + n2;
    }

    /* subtract n2 from n1 */
    static double subtract(double n1, double n2)
    {
	if (n2 > n1) {
	    return n2 - n1;
	}
        return n1 - n2;
    }

    /* multiply n1 by n2 */
    static double multiply(double n1, double n2)
    {
        return n1 * n2;
    }

    /* divide n1 by n2 */
    static double divide(double n1, double n2)
    {
	if (n2 == 0) {
	    return 0;
	}
        return n1 / n2;
    }

    /* calculate the absolute value of n */
    static double abs(double n)
    {
        if (n <= -0.0){
	    n = -n;
	}
        return n;
    }

    /* raise n to the power of m */
    static double power(double n, int m)
    {
        double result = 0;
        for (int i = 0; i < m; ++i){
	    result += n * m;
	}

	if (m <= -1) {
	    result = 1.0 / m;
	}
        return result;
    }
}
